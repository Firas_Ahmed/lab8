#include "CustomerCounter.h"

CustomerCounter::CustomerCounter(int max_cust){
    this->max_cust = max_cust;
    this->cust_count = 0;
}

void CustomerCounter::add(int cust_count){
    if(cust_count >= max_cust){
        this->cust_count = max_cust;
    }else{
        this->cust_count = cust_count + 1;
    }
}

void CustomerCounter::subtract(int cust_count){
    if(cust_count <= 0){
        this->cust_count = 0;
    }else{
        this->cust_count = cust_count - 1;
    }
}

int CustomerCounter::custCount(){
    return cust_count;
}
