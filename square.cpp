#include "shapes.h"
#ifndef _SQUARE_H
#define _SQUARE_H

class Square : public Shapes{
    private:
        double side;
    
    public:
        Square(double side){
            this->side = side;
        }
        double area(){
            return side*side;
        }
};

#endif
