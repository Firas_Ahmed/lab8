#include <iostream>
#include <vector>
#include "shapes.h"
#include "square.cpp"
#include "rectangle.cpp"
#include "circle.cpp"
int main() {
    std::vector<Shapes*> shapes = {};
    shapes.push_back(new Square(5));
    shapes.push_back(new Square(5.5));

    shapes.push_back(new Rectangle(5,4));
    shapes.push_back(new Rectangle(2.5,3.5));

    shapes.push_back(new Circle(3));
    shapes.push_back(new Circle(3.75));

    for(int i = 0; i < shapes.size(); i++){
        std :: cout << "area of shape " << i << " is " << shapes[i]->area() <<"\n";
    }
}
