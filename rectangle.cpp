#include "shapes.h"
#ifndef _RECTANGLE_H
#define _RECTANGLE_H

class Rectangle : public Shapes{
    private:
        double width;
        double length;
    public:
        Rectangle(double width, double length){
            this->width = width;
            this->length = length;
        }
        double area(){
            return width*length;
        }
};

#endif
