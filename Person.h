#include<string>
#include <vector>

#ifndef _PERSON
#define _PERSON

class Person {
    private:
        std::string name;
        std::vector<Person*> friends;

    public:
        Person(std::string);
        void befriend(Person* person);
        void unfriend(Person* person);
        void printPers();
};
#endif
