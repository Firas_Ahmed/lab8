#ifndef _CUSTOMERCOUNTER_H
#define _CUSTOMERCOUNTER_H
class CustomerCounter{
    private:
        int max_cust;
        int cust_count;

    public:
        CustomerCounter(int max_cust);
        void add(int cust_count);
        void subtract(int cust_count);
        int custCount();
};
#endif
