#ifndef _SHAPES_H
#define _SHAPES_H

class Shapes{
    public:
        virtual ~Shapes(){};
        virtual double area(){return 0;};
};

#endif
