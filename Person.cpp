#include<string>
#include<vector>
#include <iostream>
#include "Person.h"

Person::Person(std::string name){
    this->name = name;
    this->friends = {};
}

void Person::befriend(Person* person){
    this->friends.push_back(person);
}

void Person::unfriend(Person* person){
    if(!friends.empty()){
        for(int i = 0; i < friends.size(); i++){
            if(friends[i] == person){
                friends.erase(friends.begin()+i);
            }else if(i == (friends.size())-1){
                std::cout << "\nFriend not found in list\n";
            }
        }
    }else{
        std::cout << "\n" << this->name <<" has no friends to unfriend\n";
    }
}

void Person::printPers(){
    std::cout << "Name: " << name << "\t";
    std::cout << "Friends: ";
    if(!friends.empty()){
        for (auto f : this->friends) {
            std::cout << f->name << " ";
        }
    }else{
        std::cout << "No friends";
    }
    std::cout << "\n";
}
