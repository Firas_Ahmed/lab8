#include "shapes.h"
#ifndef _CIRCLE_H
#define _CIRCLE_H

class Circle : public Shapes{
    private: 
        double radius;
    public:
        Circle(double radius){
            this->radius = radius;
        }
        double area(){
            return 3.1415 * radius * radius;
        }
};

#endif
