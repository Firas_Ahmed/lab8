#include <iostream>
#include <vector>
#include "Person.h"
int main() {
    Person a = Person("John");
    Person b = Person("Jake");
    Person c = Person("Jerome");
    Person d = Person("Tim");
    
    a.befriend(&b);
    a.befriend(&c);
    a.befriend(&d);

    b.befriend(&a);
    b.befriend(&d);
    c.befriend(&b);
    d.befriend(&a);
    d.befriend(&b);

    std::cout << "before unfriending\n";
    a.printPers();
    b.printPers();
    c.printPers();
    d.printPers();

    a.unfriend(&d);
    d.unfriend(&c);
    d.unfriend(&a);
    d.unfriend(&b);
    d.unfriend(&a);
    b.unfriend(&d);
    
    std::cout << "\nafter unfriending\n";
    a.printPers();
    b.printPers();
    c.printPers();
    d.printPers();
}
